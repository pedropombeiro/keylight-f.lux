import needle from 'needle'
import { getWarmth, Position } from './lib/warmth'
import { stages } from './lib/stages'
import { getTemperature, getKelvin, LightsResponse } from './lib/elgato-keylight'

const myLocation: Position = {
  lat: 46.61877,
  long: -7.0549883,
}
const intervalInMinutes = 5
const lightsEndpoint = 'http://keylight:9123/elgato/lights'

async function adaptLights() {
  try {
    let res = await needle('get', lightsEndpoint)

    if (res.statusCode != 200) {
        console.log(`GET ${lightsEndpoint}: HTTP ${res.statusCode} ${res.statusMessage}`)
        return true
    }

    const tempRatio = getWarmth(stages, myLocation, new Date(Date.now()))
    const rawTemp = getTemperature(tempRatio)

    let lightsResponse: LightsResponse = res.body
    lightsResponse.lights.forEach((light) => {
      light.temperature = rawTemp
    })

    await needle('put', lightsEndpoint, lightsResponse, { json: true })
    console.log(`  Temperature set to ${getKelvin(rawTemp)}K`)
  } catch (error) {
    console.log(error)
    return false
  }

  return true
}

function sleep(millis: number) {
  return new Promise(resolve => setTimeout(resolve, millis))
}

async function adaptLightsWithRetry() {
  let ok = false
  for (let tries = 0; !ok && tries < 3; ++tries) {
    if (tries != 0) {
      console.log(`${new Date().toISOString()}: Waiting for 5 seconds...`)
      await sleep(5*1000)
    }

    console.log(`${new Date().toISOString()}: Adapting lights (attempt #${tries + 1})...`)
    ok = await adaptLights()
  }
}

function run() {
  adaptLightsWithRetry()

  setInterval(adaptLightsWithRetry, intervalInMinutes*60*1000)
}

const args = process.argv.slice(2)

switch (args[0]) {
  case '-s':
    adaptLightsWithRetry()
    break
  case '-h':
  case '--help':
    console.log('usage: keylight-f.lux [-s|-h|--help]')
    console.log('')
    console.log('    -s:          Single-shot run')
    console.log('    -h, --help:  Display this help screen')
    console.log('')
    break
  default:
    run()
    break
}
