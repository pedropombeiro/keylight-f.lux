import {Stages} from './warmth'

const stages: Stages = {
  toSunrise: [
    {
      threshold: 0.6,
      warmth: 50,
    },
    {
      threshold: 0.8,
      warmth: 70,
    },
    {
      threshold: 1.1,
      warmth: 100,
    },
  ],
  toSunset: [
    {
      threshold: 0.90,
      warmth: 0,
    },
    {
      threshold: 1,
      warmth: 20,
    },
    {
      threshold: 1.1,
      warmth: 40,
    },
    {
      threshold: 1.2,
      warmth: 80,
    },
  ],
}

export { stages }
