interface Light {
  on: boolean
  temperature: number
  brightness: number
}

export interface LightsResponse {
  numberOfLights: number
  lights: Array<Light>
}

const minKelvin = 2900
const maxKelvin = 7000

function getTemperature(tempRatio: number) {
  return fromKelvinToRawTemp(fromRatioToKelvin(tempRatio))
}

function fromRatioToKelvin(tempRatio: number) {
  return Math.round(maxKelvin - (tempRatio / 100) * (maxKelvin-minKelvin))
}

function fromKelvinToRawTemp(kelvin: number) {
  return Math.round(1000000/kelvin)
}

function getKelvin(rawTemp: number) {
  return Math.round(1000000/rawTemp)
}

export { getTemperature, getKelvin }
