// Source: https://github.com/overwatchcorp/tradfri-flux

import sunCalc from 'suncalc'

interface SunPositionStage {
  threshold: number
  warmth: number
}

export interface Stages {
  toSunrise: Array<SunPositionStage>
  toSunset: Array<SunPositionStage>

}

export interface Position {
  lat: number
  long: number
}

function parseDateISOString(s: string) {
  let ds = s.split(/\D+/).map((s: string) => parseInt(s))
  ds[1] = ds[1] - 1; // adjust month
  return new Date(ds[0], ds[1], ds[2], ds[3], ds[4], ds[5], ds[6])
}

const getWarmth = (stages: Stages, {lat, long}: Position, date: Date) => {
  // check to make sure that date argument is actually a date
  if (!(date instanceof Date) || typeof date.getTime !== 'function') {
    throw new Error('not an instance of Date');
  // check validity of date instance
  } else if (isNaN(date.getTime())) throw new Error('invalid date');

  // get the sunrise and sunset values based on the time at noon today
  const today = new Date(
    // TODO: look at time zone offset
    Date.parse(`${date.toLocaleDateString()} 12:00:00`)
  );
  let {sunrise, sunset} = sunCalc.getTimes(today, lat, long);
  sunrise = parseDateISOString(sunrise.toISOString())
  sunset = parseDateISOString(sunset.toISOString())

  let warmth = 0;
  let closestLowerBound = -1;
  let closestLowerBoundValue = 0;
  if (date.getTime() < sunrise.getTime()) {
    // convert current time to % before sunrise
    const minutesIntoDay = (date.getHours() * 60) + date.getMinutes();
    const sunriseInMinutes = (sunrise.getHours() * 60) + sunrise.getMinutes();
    const percentToSunrise = minutesIntoDay / sunriseInMinutes;

    // if a value is not found for pre-sunrise stages, set warmth to
    // last value of sunset values
    const {toSunrise} = stages;
    toSunrise.map((stage, index) => {
      if (
        percentToSunrise > stage.threshold &&
        stage.threshold > closestLowerBoundValue
      ) {
        closestLowerBound = index;
        closestLowerBoundValue = stage.threshold;
        warmth = stage.warmth;
      }
    });
    if (closestLowerBound < 0) {
      warmth = stages.toSunset[stages.toSunset.length - 1].warmth;
    } else if (isWithinStages(toSunrise, closestLowerBound, percentToSunrise)) {
      warmth = calculateValueBetweenStages(toSunrise, closestLowerBound, percentToSunrise)
    }
  } else {
    // convert current time to % before or after sunset
    const minutesIntoDay = (date.getHours() * 60) + date.getMinutes();
    const sunsetInMinutes = (sunset.getHours() * 60) + sunset.getMinutes();
    const percentToSunset = minutesIntoDay / sunsetInMinutes;

    const {toSunset} = stages;
    toSunset.map((stage, index) => {
      if (
        percentToSunset > stage.threshold &&
        stage.threshold > closestLowerBoundValue
      ) {
        closestLowerBound = index;
        closestLowerBoundValue = stage.threshold;
        warmth = stage.warmth;
      }
    });
    if (isWithinStages(toSunset, closestLowerBound, percentToSunset)) {
      warmth = calculateValueBetweenStages(toSunset, closestLowerBound, percentToSunset)
    }
  }
  return warmth;
};

function isWithinStages(stages: Array<SunPositionStage>, closestLowerBound: number, percentToEvent: number) {
  return closestLowerBound >= 0 &&
    stages[closestLowerBound + 1] &&
    percentToEvent > stages[closestLowerBound].threshold
}

function calculateValueBetweenStages(stages: Array<SunPositionStage>, closestLowerBound: number, percentToEvent: number) {
  const closestNextStage = stages[closestLowerBound + 1];
  const closestLowerStage = stages[closestLowerBound];
  const percentBetweenStages = (percentToEvent - closestLowerStage.threshold) /
    (closestNextStage.threshold - closestLowerStage.threshold);
  const valueBetweenStages = ((closestNextStage.warmth - closestLowerStage.warmth) * percentBetweenStages) +
    closestLowerStage.warmth;
  return Math.floor(valueBetweenStages);
}

export { getWarmth }
