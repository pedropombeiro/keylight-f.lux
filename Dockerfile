FROM node:lts-alpine
COPY /dist /app/dist
COPY /package.json /yarn.lock /app/
RUN apk add --no-cache dumb-init && \
  yarn install --cwd /app --frozen-lockfile --cache-folder /tmp && \
  rm -rf /tmp
ENTRYPOINT ["/usr/bin/dumb-init"]
CMD ["node", "/app/dist/app.js"]
