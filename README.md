# Using keylight-f.lux

## Configuration

1. Update the `myLocation` variable in `app.ts` with your (rough) GPS position;
1. Update the `lightsEndpoint` variable with the correct hostname/IP address of your Elgato Key Light;
1. Optional: update the `stages` variable in `lib/stages.ts` with your personalized values regarding transitions during sunrise and sunset.

## Deployment

- Shell environment:

    ```shell
    # Compile TypeScript into JavaScript
    yarn build

    # Run the app
    ./keylight-f.lux
    ```

- Docker environment:

    ```shell
    yarn dockerfile
    # Use your own TZ value (see https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
    docker run -d --restart always --name keylight-f.lux -e TZ=Europe/Zurich pombeirp/keylight-f.lux
    ```
